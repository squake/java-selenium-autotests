package lab3;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Lab3 {

    private static final String URL = "http://www.google.com/";
    private static final String PATH_TO_URL = "//div[@class='r']/a";
    private static final String DRIVER_PATH = "C:\\Users\\User\\Downloads\\geckodriver-v0.26.0-win64\\geckodriver.exe";
    private static final String SEARCH_TEXT = "Тестирование поисковых систем";

    private static WebDriver driver;
    private static WebDriverWait wait;

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", DRIVER_PATH);
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();

        driver.get(URL);

        wait.until(ExpectedConditions.elementToBeClickable(By.name("q")));
        driver.findElement(By.name("q")).sendKeys(SEARCH_TEXT + Keys.ENTER);

        List<WebElement> refList = driver.findElements(By.xpath(PATH_TO_URL));
        List<String> sites = refList.stream()
                .map(e -> e.getAttribute("href"))
                .collect(Collectors.toList());

        Map<Integer, Integer> tabs = new LinkedHashMap<>();
        AtomicInteger tabPosition = new AtomicInteger();
        sites.forEach(site -> {
            openNewTab(site, tabs, tabPosition.incrementAndGet());
        });

        final Map<Integer, Integer> sorted = sortByValue(tabs);
        List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        System.out.println("Closed:");
        sorted.forEach((k, v) -> {
            driver.switchTo().window(browserTabs.get(k));
            driver.close();
            System.out.println("position : " + k + " >\t" + "occurrences : " + v);
        });

        driver.quit();
    }

    private static void find(Map<Integer, Integer> sortedTabs, int position) {
        WebElement body = driver.findElement(By.tagName("body"));
        String bodyText = body.getText();

        int count = 0;
        while (bodyText.contains(SEARCH_TEXT)) {
            count++;
            bodyText = bodyText.substring(bodyText.indexOf(SEARCH_TEXT) + SEARCH_TEXT.length());
        }
        sortedTabs.put(position, count);
        System.out.println("Occurrences of '" + SEARCH_TEXT + "' found: " + count);
    }

    private static void openNewTab(String url, Map<Integer, Integer> sortedTabs, int position) {
        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        System.out.println("tabs : " + tabs.size() + " >position: " + position + " >\t" + url);
        driver.switchTo().window(tabs.get(position));
        driver.get(url);
        find(sortedTabs, position);
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
}
