package lab1;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Lab1 {

    private static final String DRIVER_PATH = "C:\\Users\\User\\Downloads\\geckodriver-v0.26.0-win64\\geckodriver.exe";

    private static WebDriver driver;

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", DRIVER_PATH);

        driver = new FirefoxDriver();
        driver.manage().window().maximize();

        File file = new Lab1().getFileFromResources("10_sites");
        List<String> sites = new ArrayList<>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            String siteEntry;
            while ((siteEntry = reader.readLine()) != null) {
                sites.add(siteEntry);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // sorted by length
        sites.sort(Comparator.comparingInt(String::length));

        AtomicInteger tabPosition = new AtomicInteger();
        Map<String, Integer> tabsSortedAlphabetically = new TreeMap<>();
        System.out.println("Opened:");
        sites.forEach(site -> {
            openNewTab(site, tabPosition.get());
            tabsSortedAlphabetically.put(driver.getTitle(), tabPosition.getAndIncrement());
        });

        List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        System.out.println("Closed:");
        tabsSortedAlphabetically.forEach((k, v) -> {
            driver.switchTo().window(browserTabs.get(v));
            driver.close();
            System.out.println("position : " + v + " >\t" + k);
        });

        driver.quit();
    }

    private static void openNewTab(String url, int position) {
        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        System.out.println("tabs : " + tabs.size() + " >position: " + position + " >\t" + url);
        driver.switchTo().window(tabs.get(position));
        driver.get(url);
    }

    private File getFileFromResources(String fileName) {
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("File is not found!");
        } else {
            return new File(resource.getFile());
        }
    }
}