package lab4;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Lab4 {
    private static final String DRIVER_PATH = "C:\\Users\\User\\Downloads\\geckodriver-v0.26.0-win64\\geckodriver.exe";
    private static final String BLOG_URL = "https://www.geeksforgeeks.org";
    private static final String PATH_TO_URL = "//span[@class='read-more']/a";
    private static final String PATH_TO_TAG = "//a[@rel='tag' or @rel='category tag']";

    private static WebDriver driver;

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", DRIVER_PATH);

        driver = new FirefoxDriver();
        driver.manage().window().maximize();

        driver.get(BLOG_URL);

        List<WebElement> refList = driver.findElements(By.xpath(PATH_TO_URL));
        List<String> refsToArticle = refList.stream()
                .map(e -> e.getAttribute("href"))
                .collect(Collectors.toList());

        AtomicInteger tabPosition = new AtomicInteger();
        refsToArticle.forEach(site -> {
            List<String> articleTags = openNewTab(site, tabPosition.incrementAndGet());
            List<String> previewTags = findTags();
            System.out.println("Preview tags: " + previewTags);
            System.out.println("Article tags: " + articleTags);
            assert previewTags.equals(articleTags);
        });

        driver.quit();
    }

    private static List<String> openNewTab(String url, int position) {
        ((JavascriptExecutor) driver).executeScript("window.open()");
        List<String> tabs = new ArrayList<>(driver.getWindowHandles());
        System.out.println("tabs : " + tabs.size() + " >position: " + position + " >\t" + url);
        driver.switchTo().window(tabs.get(position));
        driver.get(url);
        return findTags();
    }

    private static List<String> findTags() {
        List<WebElement> tagList = driver.findElements(By.xpath(PATH_TO_TAG));
        return tagList.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

}
