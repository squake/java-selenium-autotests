package lab2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Lab2 {

    private static final String URL = "http://abrams.ru/";
    private static final String DRIVER_PATH = "C:\\Users\\User\\Downloads\\geckodriver-v0.26.0-win64\\geckodriver.exe";
    private static final String PATH_TO_ELEMENT = "/html/body/div[1]/div/div/div/div/div[1]/p";

    private static WebDriver driver;

    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", DRIVER_PATH);

        driver = new FirefoxDriver();
        driver.manage().window().maximize();

        driver.get(URL);

        String text = driver.findElement(By.xpath(PATH_TO_ELEMENT)).getText();
        System.out.println(text);

        int numberOfSymbols = 0;
        int numberOfSpaces = 0;
        int i = 0;
        while (i < text.length()) {
            char letter = text.charAt(i);
            if (Character.isSpaceChar(letter)) {
                numberOfSpaces++;
            }
            if (Character.isLetterOrDigit(letter)) {
                numberOfSymbols++;
            }
            i++;
        }
        int numberOfSigns = text.length() - numberOfSpaces - numberOfSymbols;

        System.out.println("Symbols: " + numberOfSymbols + "\n" +
                "Spaces: " + numberOfSpaces + "\n" +
                "Signs: " + numberOfSigns + "\n");

        assert numberOfSymbols == 224;
        assert numberOfSpaces == 39;
        assert numberOfSigns == 6;

        driver.quit();
    }
}